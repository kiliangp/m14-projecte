# M14-REPTE8. DOCKER COMPOSE: iniciació

Implementar els següents exemples de docker-compose:
* Exemple web + portainer
* Exemple net + portainer
* Exemple web + net + portainer
* Exemple postgres + adminer
* Exemple ldap + phpldapadmin

## DOCKER-COMPOSE.YML
```
version: "2"
services: 
  web:
    image: kiliangp/web23:base
    container_name: web.edt.org
    hostname: web.edt.org
    ports:
      - "80:80"
    networks:
      - mynet
  net:
    image: kiliangp/repte7-net:latest
    container_name: net.edt.org
    hostname: net.edt.org
    ports:
      - "7:7"
      - "13:13"
      - "19:19"
    networks:
      - mynet
  portainer:
    image: portainer/portainer
    ports:
      - "9000:9000"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
      - mynet

networks:
  mynet:
```
* Utilitzem el fitxer **yaml** per fer el "docker-compose amb la següent ordre (l'engeguem):
```
docker compose -f docker-compose.yml up -d
```
<br>
Per apagar les dues imatges:

```
docker compose down
```

## COMPROVACIONS

1. **PRIMERA IMATGE**

```
En el navegador:
  http://localhost
    - Sortirà la pàgina web creada
```
2. **SEGONA IMATGE**


```
docker ps -> podem veure les imatges que estàn enceses i comprovem si funciona correctament.

docker exec -it web.edt.org /bin/bash -> entrem dintre del container encès

root@web:/opt/docker# telnet localhost 13
  Trying 127.0.0.1...
  Connected to localhost.
  Escape character is '^]'.
  31 OCT 2023 16:57:55 UTC
  Connection closed by foreign host.
```

3. **TERCERA IMATGE**

```
En el navegador:
  http://localhost:9000
    - Sortirà la pàgina web de portainer
```
