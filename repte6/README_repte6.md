# M14-REPTE 6. NETWORKS, VARIABLES I SECRETS

* Implementar un servidor LDAP on el nom de l'administrador de la base de dades de l'escola sigui captat a través de variables d'entorn.
* Implementar un servidor LDAP on el nom de l’administrador de la base de dades de l’escola sigui captat a través de docker secrets.

## DOCKERFILE
```
# Repte 6
FROM debian:latest
LABEL version="1.0"
LABEL author="Kilian Steven Guanoluisa Pionce"
LABEL subject="Repte-6"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd less
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x  /opt/docker/startup.sh
ENV LDAP_ROOTDN="kilian"
ENV LDAP_ROOTPW="kilian_passwd"
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389	
```
## STARTUP.SH
```
#!/bin/bash

# 1) Substituim el super usuari "Manager" per la variable $LDAP_ROOTDN, variable que obtindrem de l'ordre "docker run..."
sed -i "s/Manager/$LDAP_ROOTDN/g" slapd.conf
# 2) Substituim la contrasenya del super usuari per la variable $LDAP_ROOTPW, variable que obtindrem de l'ordre "docker run..."
sed -i "s/secret/$LDAP_ROOTPW/g" slapd.conf
# 3) Esborrar els directoris de configuració i de dades
rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
# 4) Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
# 5) Injectar a bajo nivel los datos de la BD 'populate' de la organización dc=edt,dc=org
slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
# 6) Asignar la propiedad y grupo del directorio de datos de configuración al usuario openldap
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
# 7) Engegar el servei slapd amb el parametre que fa debug per mantenir-ho engegat en foreground
/usr/sbin/slapd -d0

```

## EDT-ORG.LDIF

```
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org

dn: ou=maquines,dc=edt,dc=org
ou: maquines
description: Container per a maquines linux
objectclass: organizationalunit

dn: ou=clients,dc=edt,dc=org
ou: clients
description: Container per a clients linux
objectclass: organizationalunit

dn: ou=productes,dc=edt,dc=org
ou: productes
description: Container per a productes linux
objectclass: organizationalunit

dn: ou=usuaris,dc=edt,dc=org
ou: usuaris
description: Container per usuaris del sistema linux
objectclass: organizationalunit

dn: cn=Pau Pou,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pau Pou
cn: Pauet Pou
sn: Pou
homephone: 555-222-2220
mail: pau@edt.org
description: Watch out for this guy
ou: Profes
uid: pau
uidNumber: 5000
gidNumber: 100
homeDirectory: /tmp/home/pau
userPassword: {SSHA}NDkipesNQqTFDgGJfyraLz/csZAIlk2/

dn: cn=Pere Pou,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pere Pou
sn: Pou
homephone: 555-222-2221
mail: pere@edt.org
description: Watch out for this guy
ou: Profes
uid: pere
uidNumber: 5001
gidNumber: 100
homeDirectory: /tmp/home/pere
userPassword: {SSHA}ghmtRL11YtXoUhIP7z6f7nb8RCNadFe+

dn: cn=Anna Pou,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Anna Pou
cn: Anita Pou
sn: Pou
homephone: 555-222-2222
mail: anna@edt.org
description: Watch out for this girl
ou: Alumnes
uid: anna
uidNumber: 5002
gidNumber: 600
homeDirectory: /tmp/home/anna
userPassword: {SSHA}Bm4B3Bu/fuH6Bby9lgxfFAwLYrK0RbOq

dn: cn=Marta Mas,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Marta Mas
sn: Mas
homephone: 555-222-2223
mail: marta@edt.org
description: Watch out for this girl
ou: Alumnes
uid: marta
uidNumber: 5003
gidNumber: 600
homeDirectory: /tmp/home/marta
userPassword: {SSHA}9+1F2f5vcW8z/tmSzYNWdlz5GbDCyoOw

dn: cn=Jordi Mas,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Jordi Mas
cn: Giorgios Mas
sn: Mas
homephone: 555-222-2224
mail: jordi@edt.org
description: Watch out for this girl
ou: Alumnes
ou: Profes
uid: jordi
uidNumber: 5004
gidNumber: 100
homeDirectory: /tmp/home/jordi
userPassword: {SSHA}T5jrMgpJwZZgu0azkLIVoYhiG08/KGUv

dn: cn=Admin System,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Administrador Sistema
cn: System Admin
sn: System
homephone: 555-222-2225
mail: anna@edt.org
description: Watch out for this girl
ou: system
ou: admin
uid: admin
uidNumber: 10
gidNumber: 10
homeDirectory: /tmp/home/admin
userPassword: {SSHA}4mS0FycWc5bkpW8/a396SGNDTQUlFSX3
```
## SLAPD.CONF

```
#
# Veure slapd.conf(5) per detalls sobre les opcions de configuració.
# Aquest arxiu NO hauria de ser llegible per a tothom.
#
# Paquets Debian: slapd ldap-utils

include         /etc/ldap/schema/corba.schema
include         /etc/ldap/schema/core.schema
include         /etc/ldap/schema/cosine.schema
include         /etc/ldap/schema/duaconf.schema
include         /etc/ldap/schema/dyngroup.schema
include         /etc/ldap/schema/inetorgperson.schema
include         /etc/ldap/schema/java.schema
include         /etc/ldap/schema/misc.schema
include         /etc/ldap/schema/nis.schema
include         /etc/ldap/schema/openldap.schema
#include                /etc/ldap/schema/ppolicy.schema
include         /etc/ldap/schema/collective.schema

# Permetre connexions de clients LDAPv2. Això NO és la configuració per defecte.
allow bind_v2

pidfile         /var/run/slapd/slapd.pid
#argsfile       /var/run/openldap/slapd.args

modulepath /usr/lib/ldap
moduleload back_mdb.la
moduleload back_monitor.la
# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
rootpw secret
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
# ----------------------------------------------------------------------
database monitor
access to *
       by dn.exact="cn=Manager,dc=edt,dc=org" read
       by * none
```
# ORDRES

```
docker build -t kiliangp/repte6:base .
docker run --rm --name ldap.edt.org -d kiliangp/repte6:base
```
* Una vegada iniciat el container en detach, l'iniciem a través d'aquesta ordre:
```
docker exec -it kiliangp/repte6:base /bin/bash
```
**Per veure si les modificacions s'han fet correctament veiem el fitxer de configuració "slapd.conf"**
* cat slapd.conf
- Podem veure que ha canviat el cn a cn=kilian i el passwd del superusuari a pw=kilian_passwd
