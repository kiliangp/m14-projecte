# M14-REPTE7-NET. CREACIÓ DE SERVIDORS: SSH, SAMBA I NET

Crear els servidors següents:
* Net: Implementa els serveis de xarxa echo, daytime, chargen, ftp, tftp i http.
* SAMBA: implementa un servidor samba stand alone.
* SSH amb autenticació contra el servidor ldap.

## DOCKERFILE
```
FROM debian:latest
LABEL author="Kilian Steven Guanoluisa Pionce"
LABEL subject="Repte7-net"
RUN apt-get update
RUN apt-get -y install xinetd iproute2 iputils-ping nmap procps net-tools apache2 tftp vsftpd ftp telnet wget vim tftpd-hpa 
COPY daytime chargen echo /etc/xinetd.d/
COPY index.html /var/www/html/
RUN mkdir /opt/docker
COPY * /opt/docker/
COPY vsftpd.conf /etc/
RUN chmod +x /opt/docker/startup.sh
RUN bash /opt/docker/useradd.sh
RUN chmod 775 /srv/ftp/
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE  7 13 19 21 69 80
```
Obrim els ports següents:
```
echo: 7
daytime: 13
chargen: 19
ftp: 21
tftp: 69
http: 80
```
## STARTUP.SH 
```
#!/bin/bash
service tftpd-hpa start
service vsftpd start
apachectl start
/usr/sbin/xinetd -dontfork
```
* Iniciem els tres serveis: http,ftp tftp; a més, activem el dimoni.

## INDEX.HTML 
```
<h1 style="text-align: center;">Repte-7-net</h1>
```
* Aquesta serà la línea de la nostra pàgina web.

## CHARGEN 
```
# default: off
# description: An xinetd internal service which generate characters.  The
# xinetd internal service which continuously generates characters until the
# connection is dropped.  The characters look something like this:
# !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefg
# This is the tcp version.
service chargen
{
	disable		= no
	type		= INTERNAL
	id		= chargen-stream
	socket_type	= stream
	protocol	= tcp
	user		= root
	wait		= no
}

# This is the udp version.
service chargen
{
	disable		= yes
	type		= INTERNAL
	id		= chargen-dgram
	socket_type	= dgram
	protocol	= udp
	user		= root
	wait		= yes
}
```

## DAYTIME 

```
# default: off
# description: An internal xinetd service which gets the current system time
# then prints it out in a format like this: "Wed Nov 13 22:30:27 EST 2002".
# This is the tcp version.
service daytime
{
	disable		= no
	type		= INTERNAL
	id		= daytime-stream
	socket_type	= stream
	protocol	= tcp
	user		= root
	wait		= no
}

# This is the udp version.
service daytime
{
	disable		= yes
	type		= INTERNAL
	id		= daytime-dgram
	socket_type	= dgram
	protocol	= udp
	user		= root
	wait		= yes
}
```

## ECHO 
```
# default: off
# description: An xinetd internal service which echo's characters back to
# clients.
# This is the tcp version.
service echo
{
	disable		= no
	type		= INTERNAL
	id		= echo-stream
	socket_type	= stream
	protocol	= tcp
	user		= root
	wait		= no
}

# This is the udp version.
service echo
{
	disable		= yes
	type		= INTERNAL
	id		= echo-dgram
	socket_type	= dgram
	protocol	= udp
	user		= root
	wait		= yes
}
```
Copiem els arxius de la pràctica de l'any passat:
* https://gitlab.com/kiliangp/web22/-/tree/main/net22:base/practica_1?ref_type=heads

## USERADD.SH

```
#! /bin/bash

for user in unix01 unix02 unix03 unix04 unix05
do
	useradd -m -s /bin/bash $user
	echo $user:$user | chpasswd
	usermod -aG ftp $user
done
```
<br>
- FITXER QUE UTILITZAREM PER CREAR USUARIS LOCALS PER AL SERVIDOR FTP

<br>

## VSFTPD.CONF
```
# Example config file /etc/vsftpd.conf
#
# The default compiled in settings are fairly paranoid. This sample file
# loosens things up a bit, to make the ftp daemon more usable.
# Please see vsftpd.conf.5 for all compiled in defaults.
#
# READ THIS: This example file is NOT an exhaustive list of vsftpd options.
# Please read the vsftpd.conf.5 manual page to get a full idea of vsftpd's
# capabilities.
#
#
# Run standalone?  vsftpd can run either from an inetd or as a standalone
# daemon started from an initscript.
listen=NO
#
# This directive enables listening on IPv6 sockets. By default, listening
# on the IPv6 "any" address (::) will accept connections from both IPv6
# and IPv4 clients. It is not necessary to listen on *both* IPv4 and IPv6
# sockets. If you want that (perhaps because you want to listen on specific
# addresses) then you must run two copies of vsftpd with two configuration
# files.
listen_ipv6=YES
#
# Allow anonymous FTP? (Disabled by default).
#anonymous_enable=YES
#
# Uncomment this to allow local users to log in.
local_enable=YES
#
# Uncomment this to enable any form of FTP write command.
write_enable=YES
#
# Default umask for local users is 077. You may wish to change this to 022,
# if your users expect that (022 is used by most other ftpd's)
local_umask=022
#
# Uncomment this to allow the anonymous FTP user to upload files. This only
# has an effect if the above global write enable is activated. Also, you will
# obviously need to create a directory writable by the FTP user.
#anon_upload_enable=YES
#
# Uncomment this if you want the anonymous FTP user to be able to create
# new directories.
#anon_mkdir_write_enable=YES
#
# Activate directory messages - messages given to remote users when they
# go into a certain directory.
dirmessage_enable=YES
#
# If enabled, vsftpd will display directory listings with the time
# in  your  local  time  zone.  The default is to display GMT. The
# times returned by the MDTM FTP command are also affected by this
# option.
use_localtime=YES
#
# Activate logging of uploads/downloads.
xferlog_enable=YES
#
# Make sure PORT transfer connections originate from port 20 (ftp-data).
connect_from_port_20=YES
#
# If you want, you can arrange for uploaded anonymous files to be owned by
# a different user. Note! Using "root" for uploaded files is not
# recommended!
#chown_uploads=YES
#chown_username=whoever
#
# You may override where the log file goes if you like. The default is shown
# below.
#xferlog_file=/var/log/vsftpd.log
#
# If you want, you can have your log file in standard ftpd xferlog format.
# Note that the default log file location is /var/log/xferlog in this case.
#xferlog_std_format=YES
#
# You may change the default value for timing out an idle session.
#idle_session_timeout=600
#
# You may change the default value for timing out a data connection.
#data_connection_timeout=120
#
# It is recommended that you define on your system a unique user which the
# ftp server can use as a totally isolated and unprivileged user.
#nopriv_user=ftpsecure
#
# Enable this and the server will recognise asynchronous ABOR requests. Not
# recommended for security (the code is non-trivial). Not enabling it,
# however, may confuse older FTP clients.
#async_abor_enable=YES
#
# By default the server will pretend to allow ASCII mode but in fact ignore
# the request. Turn on the below options to have the server actually do ASCII
# mangling on files when in ASCII mode.
# Beware that on some FTP servers, ASCII support allows a denial of service
# attack (DoS) via the command "SIZE /big/file" in ASCII mode. vsftpd
# predicted this attack and has always been safe, reporting the size of the
# raw file.
# ASCII mangling is a horrible feature of the protocol.
#ascii_upload_enable=YES
#ascii_download_enable=YES
#
# You may fully customise the login banner string:
#ftpd_banner=Welcome to blah FTP service.
#
# You may specify a file of disallowed anonymous e-mail addresses. Apparently
# useful for combatting certain DoS attacks.
#deny_email_enable=YES
# (default follows)
#banned_email_file=/etc/vsftpd.banned_emails
#
# You may restrict local users to their home directories.  See the FAQ for
# the possible risks in this before using chroot_local_user or
# chroot_list_enable below.
#chroot_local_user=YES
#
# You may specify an explicit list of local users to chroot() to their home
# directory. If chroot_local_user is YES, then this list becomes a list of
# users to NOT chroot().
# (Warning! chroot'ing can be very dangerous. If using chroot, make sure that
# the user does not have write access to the top level directory within the
# chroot)
#chroot_local_user=YES
#chroot_list_enable=YES
# (default follows)
#chroot_list_file=/etc/vsftpd.chroot_list
#
# You may activate the "-R" option to the builtin ls. This is disabled by
# default to avoid remote users being able to cause excessive I/O on large
# sites. However, some broken FTP clients such as "ncftp" and "mirror" assume
# the presence of the "-R" option, so there is a strong case for enabling it.
#ls_recurse_enable=YES
#
# Customization
#
# Some of vsftpd's settings don't fit the filesystem layout by
# default.
#
# This option should be the name of a directory which is empty.  Also, the
# directory should not be writable by the ftp user. This directory is used
# as a secure chroot() jail at times vsftpd does not require filesystem
# access.
secure_chroot_dir=/var/run/vsftpd/empty
#
# This string is the name of the PAM service vsftpd will use.
pam_service_name=vsftpd
#
# This option specifies the location of the RSA certificate to use for SSL
# encrypted connections.
rsa_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem
rsa_private_key_file=/etc/ssl/private/ssl-cert-snakeoil.key
ssl_enable=NO

#
# Uncomment this to indicate that vsftpd use a utf8 filesystem.
#utf8_filesystem=YES
#anon_other_write_enable=YES
#anon_umask=022
ftpd_banner=Aquest es el servidor ftp de Kilian!!!
local_root=/srv/ftp/
```
* **FITXER DE CONFIGURACIÓ DEL SERVIDOR FTP** <br>
Les últimes línees d'aquest fitxer serviran perquè els usuaris locals es puguin connectar amb la contrasenya que hem creat al fitxer "useradd.sh". Aquest fitxer el podem extreure al iniciar al container amb els serveis instalats, només hem de copiar les línees anteriors i afegir-ne de noves. 
## ORDRES
```
docker build -t kiliangp/repte7-net .
docker run --rm --name net -p 7:7 -p 13:13 -p 19:19  -p 20:20 -p 21:21 -p 69:69 -p 80:80 -d kiliangp/repte7-net
```
Engeguem el container en background, però el servei estarà en foreground.
* **docker exec -it net /bin/bash**

Dintre del container:

```
ps ax -> per veure els processos que s'estan executant
cat /var/www/html/index.html -> per veure la nostra pàgina web
--
telnet localhost 13 -> daytime
telnet localhost 19 -> chargen
telnet localhost 7 -> echo
telnet localhost 80 -> http
	O
telnet 172.17.0.2 13
telnet 172.17.0.2 19
telnet 172.17.0.2 7
telnet 172.17.0.2 80
	- Totes 4 tindran connexió amb el localhost
```
## Per veure si funciona el servei http:

* http://localhost
* http://172.17.0.2
Totes dues serviran per veure la nostra pàgina web gràcies a que estem propagant pel port 80.

## Per veure si funciona el servei ftp:

```
Dintre del container:
######################################
root@net:/opt/docker# ftp 172.17.0.2
	Connected to 172.17.0.2.
	220 Aquest es el servidor ftp de Kilian!!!
	Name (172.17.0.2:root): unix02
	331 Please specify the password.
	Password: 			#unix02
	230 Login successful.
	Remote system type is UNIX.
	Using binary mode to transfer files.
	ftp> 
#####################################
Fora del container:
sudo apt-get install -y tftp ftp
tftp 172.17.0.2
	tftp> 
```




