# M14-SERVIDOR LDAP AMB PERSISTÈNCIA DE DADES
* **V1**. Implementar una imatge que actuï com a servidor ldap amb persistència de dades tant de configuració com les dades de la base de dades usuaris.
 Hem de crear 4 documents. **Dockerfile**,**edt-org.ldif**,**slapd.conf**,**startup.sh**
1. **vim Dockerfile**
* En aquest fitxer el que fem és actualitzar i instalar els paquets de ldap, a més de copiar tots els fitxers que tenim en el directori i posar-ho dintre del container en un directori creat també amb el Dockerfile (RUN mkdir ...).
```
CMD /opt/docker/startup.sh 
EXPOSE 389
```
2. **vim startup.sh**
* Introduïm les comandes necesàries que borraren el directori de configuració dinàmica a partir del fitxer slapd.conf, injectar a baix nivell les dades de la BD, asignar la propietat i el grup de les dades de configuració a l'usuari openldap, i encendre el dimoni.
3. **vim slapd.conf**
* Fitxer de configuració que substituirà el fitxer de configuració dinàmica que amb el startup l'eliminarà.
4. **vim edt-org.ldif**
* Document que el seu interior posseeix una base de dades que és el que injectarem a baix nivell amb l'ordre **slapadd**.
5. **docker volume create repte4-config ; docker volume create repte4-dades**
6. **docker build -t repte4-volums .**
* Creem  una imatge a partir del Dockerfile i als altres documents.
7. **docker run --rm -v repte4-dades:/var/lib/ldap -v repte4-config:/etc/ldap/slapd.d --name repte4-dades -d repte4-volums**
* Un volum apunta cap al directori de configuració dinàmica (/etc/ldap/slapd.d) i un altre cap on es troba la base de dades per defecte (/var/lib/ldap)
8. **docker exec -it repte4-dades /bin/bash**
* Entrem dintre del container que hem creat.
9. **ldapsearch -x -LLL -b 'dc=edt,dc=org'**
* Fem una prova per a comprovar que la base de dades funciona correctament.
10. **vim kill.ldif**
* cn=Jordi Mas,ou=usuaris,dc=edt,dc=org
* cn=Marta Mas,ou=usuaris,dc=edt,dc=org
11. **ldapdelete -x -D 'cn=Manager,dc=edt,dc=org' -w secret -f kill.ldif**
* Eliminem dos usuaris a través d'un fitxer.
12. Sortim de la sessió, parem el container amb un **docker stop repte4-volums** i eliminem la imatge, amb un **docker rmi ...**
* L'esborrem per després regenerar-la i veure si la persistència ha funcionat correctament amb els seus volums respectius.
13. **docker build -t repte4-volums**
14. **docker run --rm -v repte4-dades:/var/lib/ldap -v repte4-config:/etc/ldap/slapd.d --name repte4-dades -d repte4-volums**
* Tornem a fer la mateixa ordre 
15. **docker exec -it repte4-volums**
16. **ldapsearch -x -LLL -b 'dc=edt,dc=org' dn**
* Podem veure que els dos usuaris que hem eliminat abans d'eliminar la imatge no estàn, efectivament hi ha hagut persistència de dades.
***
**ORDRES PER PUJAR LA IMATGE**
1. **docker tag repte4-volums kiliangp/repte4-volums**
2. **docker push kiliangp/repte4-volums**
* Pugem la imatge
***
