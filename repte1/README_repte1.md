# M14-PROJECTE


# Debian 22

Exemple de crear una imatge basada en Debian el curs 2022-2023.

Imatge:
* **Versió 1**. Incorpora els paquets nmap, tree, vim, procps i iproute2.

### Execució:
1. **sudo usermod -aG docker guest**
* Associem el grup docker a l'usuari guest perquè no ens surti cap error i tingui els permissos necessaris.
2. **sudo newgrp**
* Fem aquesta ordre perquè l'usuari guest entri de manera temporal al grup de Docker.
3. **docker run --rm -it kiliangp/debian22:latest /bin/bash**
* Creem un container que s'esborri quan sortim de la sessió, amb un sistema operatiu Debian, i a més que sigui interactiu.
4. **vim DockerFile**
#### **Dintre del Dockefile**
1. **FROM debian:latest**
* Indiquem quin debian estem utilitzant
2. **LABEL author="@edt Kilian repte_1"**
* Indiquem l'autor d'aquest Docker
3. **RUN apt-get update**
* Ordre que farà automàticament una begada s'inicialitzi el container, per actualitzar els paquets dessitjats.
4. **RUN apt-get install -y procps nmap tree vim**
* Instalem els paquets mínims per a la pràctica.
#### **Fora del Dockerfile**
1. **docker build -t kiliangp/debian22:latest .**
* Creamos una imagen a partir del container.
2. **docker tag kiliangp/debian22:latest kiliangp/repte1_container**
* Cambiem el nom per identificar-ho en el repositori de DockerHub
3. **docker push kiliangp/repte1_container**
* Pugem la imatge al nostre repositori Docker
