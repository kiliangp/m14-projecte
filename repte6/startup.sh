#!/bin/bash
#
# 1) Substituim el super usuari "Manager" per la variable $LDAP_ROOTDN, variable que obtindrem de l'ordre "docker run..."
sed -i "s/Manager/$LDAP_ROOTDN/g" slapd.conf
# 2) Substituim la contrasenya del super usuari per la variable $LDAP_ROOTPW, variable que obtindrem de l'ordre "docker run..."
sed -i "s/secret/$LDAP_ROOTPW/g" slapd.conf
# 3) Esborrar els directoris de configuració i de dades
rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
# 4) Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
# 5) Injectar a bajo nivel los datos de la BD 'populate' de la organización dc=edt,dc=org
slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
# 6) Asignar la propiedad y grupo del directorio de datos de configuración al usuario openldap
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
# 7) Engegar el servei slapd amb el parametre que fa debug per mantenir-ho engegat en foreground
/usr/sbin/slapd -d0

