# M14-REPTE1. GET STARTED WITH SWARM
* Implementar l'exemple "Get Started with Swarm"

## DOCKER SWARM INIT

```
* docker swarm init -> inicia un nou clúster Swarm en el node actual. El node en el que executes aquesta comanda es converteix en el "node administrador" del clúster.
* --advertise-addr 10.200.243.203 -> especifica la dirección a la que nos debemos conectar.

$ docker swarm init --advertise-addr 10.200.243.203
Swarm initialized: current node (yxm6wceq9bqd24a3affzz8iav) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-4vtk9dpw5s6g29q4ror5sbxut8shwffpq42qrrmwasbi8vc92g-1518q2p4v7r5ss8zuuutpcznu 10.200.243.203:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

## DOCKER INFO 
```
$ docker info -> ordre que s'utilitza per obtenir informació detallada sobre l'entorn de Docker en el sistema: versió de Docker, quantitat de contenidors, quantita d'imatges, informació sobre la xarxa, etc. 

Client: Docker Engine - Community
 Version:    24.0.7
 Context:    default
 Debug Mode: false
 Plugins:
  buildx: Docker Buildx (Docker Inc.)
    Version:  v0.11.2
    Path:     /usr/libexec/docker/cli-plugins/docker-buildx
  compose: Docker Compose (Docker Inc.)
    Version:  v2.21.0
    Path:     /usr/libexec/docker/cli-plugins/docker-compose
substack
  Paused: 0
  Stopped: 16
 Images: 21
 Server Version: 24.0.7
 Storage Driver: overlay2
  Backing Filesystem: extfs
  Supports d_type: true
  Using metacopy: false
  Native Overlay Diff: true
  userxattr: false
 Logging Driver: json-file
 Cgroup Driver: systemd
 Cgroup Version: 2
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
 Swarm: active
  NodeID: yxm6wceq9bqd24a3affzz8iav
  Is Manager: true
  ClusterID: mlcf7a4k0q056jiws2ffr8ixl
  Managers: 1
  Nodes: 1
  Default Address Pool: 10.0.0.0/8  
  SubnetSize: 24
  Data Path Port: 4789
  Orchestration:
   Task History Retention Limit: 5
  Raft:
   Snapshot Interval: 10000
   Number of Old Snapshots to Retain: 0
   Heartbeat Tick: 1
   Election Tick: 10
  Dispatcher:
   Heartbeat Period: 5 seconds
  CA Configuration:
   Expiry Duration: 3 months
   Force Rotate: 0
  Autolock Managers: false
  Root Rotation In Progress: false
  Node Address: 10.200.243.203
  Manager Addresses:
   10.200.243.203:2377
 Runtimes: io.containerd.runc.v2 runc
 Default Runtime: runc
 Init Binary: docker-init
 containerd version: 61f9fd88f79f081d64d6fa3bb1a0dc71ec870523
 runc version: v1.1.9-0-gccaecfc
 init version: de40ad0
 Security Options:
  apparmor
  seccomp
   Profile: builtin
  cgroupns
 Kernel Version: 6.1.0-12-amd64
 Operating System: Debian GNU/Linux 12 (bookworm)
 OSType: linux
 Architecture: x86_64
 CPUs: 8
 Total Memory: 15.49GiB
 Name: i03
 ID: 10d5c6b5-2db9-4bd2-b202-21cfb067a732
 Docker Root Dir: /var/lib/docker
 Debug Mode: false
 Username: kiliangp
 Experimental: false
 Insecure Registries:
  127.0.0.0/8
 Live Restore Enabled: false
```

## DOCKER NODE LS 

```
$ docker node ls -> s'utilitza per llistar els nodes en un clúster de Docker Swarm.
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
yxm6wceq9bqd24a3affzz8iav *   i03        Ready     Active         Leader           24.0.7
```

## DOCKER SWARM JOIN-TOKEN WORKER 

```
$ docker swarm join-token worker -> genera un token que utilitzarà el treballador, aquest executarà les tasqures dels contenidors.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-304isaeugshcswx9n60lq4lmt0gxosrquv79g8y4lulakps4vq-8o7z86rhcraa2qlgxgeq515ly 10.200.243.203:2377

```

## DOCKER SWARM JOIN-TOKEN MANAGER

```
$ docker swarm join-token manager -> genera un token que utilitzarà el manager, aquest serpa el respondable de l'administració

To add a manager to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-4vtk9dpw5s6g29q4ror5sbxut8shwffpq42qrrmwasbi8vc92g-90uk1u6li8baf113cez60q5ea 10.200.243.203:2377

```

## ADD NODES TO A SWARM

```
ORDRE QUE FEM A L'ORDINADOR i04 (WORKER)

$ docker swarm join --token SWMTKN-1-304isaeugshcswx9n60lq4lmt0gxosrquv79g8y4lulakps4vq-8o7z86rhcraa2qlgxgeq515ly 10.200.243.203:2377

This node joined a swarm as a worker
```

```
ORDRE QUE FEM A L'ORDINADOR MANAGER (i03)

$ docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
z9bsjwd7s2xdiwme3nbn9hcm9 *   i03        Ready     Active         Leader           24.0.7
myoelbs5mpw3ywszi45awi057     i04        Ready     Active                          24.0.2

```


**En el ordinador MANAGER**

```
$ docker service create --replicas 1 --name helloworld alpine ping docker.com -> li estem dient que crei un nou servei, que faci una rèplica, que s'anomeni "helloworld", utilitzem l'imatge alpine, i volem que quan iniciï faci l'ordre "ping docker.com"

2sbgqqwxkwt6z7j1812hhdajq
overall progress: 1 out of 1 tasks 
1/1: running   [==================================================>] 
verify: Service converged


$ docker service ls -> llistem els serveis que tenim activats
ID             NAME         MODE         REPLICAS   IMAGE           PORTS
xazlztmsd2pz   helloworld   replicated   1/1        alpine:latest  

$ docker service ps helloworld -> llistem els serveis que estàn actius al servei "helloworld"
ID             NAME           IMAGE           NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
hoyz756zqg7d   helloworld.1   alpine:latest   i03       Running         Running 2 minutes ago 
```

## INSPECT THE SERVICE

```
$ docker service inspect --pretty helloworld -> s'utilitza per obtenir informació detallada, "--pretty" modifica la sortida perqueè sigui més llegible 

ID:		2sbgqqwxkwt6z7j1812hhdajq
Name:		helloworld
Service Mode:	Replicated
 Replicas:	1
Placement:
UpdateConfig:
 Parallelism:	1
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Update order:      stop-first
RollbackConfig:
 Parallelism:	1
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Rollback order:    stop-first
ContainerSpec:
 Image:		alpine:latest@sha256:eece025e432126ce23f223450a0326fbebde39cdf496a85d8c016293fc851978
 Args:		ping docker.com 
 Init:		false
Resources:
Endpoint Mode:	vip

-----------------------------------------------------------

$ docker service inspect helloworld -> inspeccionem el servei "helloworld"
[
    {
        "ID": "2sbgqqwxkwt6z7j1812hhdajq",
        "Version": {
            "Index": 16
        },
        "CreatedAt": "2023-11-23T11:25:31.724967019Z",
        "UpdatedAt": "2023-11-23T11:25:31.724967019Z",
        "Spec": {
            "Name": "helloworld",
            "Labels": {},
            "TaskTemplate": {
                "ContainerSpec": {
                    "Image": "alpine:latest@sha256:eece025e432126ce23f223450a0326fbebde39cdf496a85d8c016293fc851978",
                    "Args": [
                        "ping",
                        "docker.com"
                    ],
                    "Init": false,
                    "StopGracePeriod": 10000000000,
                    "DNSConfig": {},
                    "Isolation": "default"
                },
                "Resources": {
                    "Limits": {},
                    "Reservations": {}
                },
                "RestartPolicy": {
                    "Condition": "any",
                    "Delay": 5000000000,
                    "MaxAttempts": 0
                },
                "Placement": {
                    "Platforms": [
                        {
                            "Architecture": "amd64",
                            "OS": "linux"
                        },
                        {
                            "OS": "linux"
                        },
                        {
                            "OS": "linux"
                        },
                        {
                            "Architecture": "arm64",
                            "OS": "linux"
                        },
                        {
                            "Architecture": "386",
                            "OS": "linux"
                        },
                        {
                            "Architecture": "ppc64le",
                            "OS": "linux"
                        },
                        {
                            "Architecture": "s390x",
                            "OS": "linux"
                        }
                    ]
                },
                "ForceUpdate": 0,
                "Runtime": "container"
            },
            "Mode": {
                "Replicated": {
                    "Replicas": 1
                }
            },
            "UpdateConfig": {
                "Parallelism": 1,
                "FailureAction": "pause",
                "Monitor": 5000000000,
                "MaxFailureRatio": 0,
                "Order": "stop-first"
            },
            "RollbackConfig": {
                "Parallelism": 1,
                "FailureAction": "pause",
                "Monitor": 5000000000,
                "MaxFailureRatio": 0,
                "Order": "stop-first"
            },
            "EndpointSpec": {
                "Mode": "vip"
            }
        },
        "Endpoint": {
            "Spec": {}
        }
    }
]

-------------------------------------------------------------
$ docker service ps helloworld -> veiem les tasques  el servei "helloworld".

ID             NAME           IMAGE           NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
hoyz756zqg7d   helloworld.1   alpine:latest   i03       Running         Running 4 minutes ago 

$ docker ps

CONTAINER ID   IMAGE           COMMAND             CREATED         STATUS         PORTS     NAMES
0ab062cf0684   alpine:latest   "ping docker.com"   4 minutes ago   Up 4 minutes             helloworld.1.hoyz756zqg7d5kghxl0yitt77

```
## CHANGE THE SCALE

```
$ docker service scale helloworld=5 ->s'utilitza per canviar el número de rèpliques d'un servei en Docker Swarm

helloworld scaled to 5
overall progress: 5 out of 5 tasks 
1/5: running   
2/5: running   
3/5: running   
4/5: running   
5/5: running   
verify: Service converged

$ docker service ps helloworld -> podem veure que el nombre de serveis ha canviat degut a l'ordre "scale"

ID             NAME           IMAGE           NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
hoyz756zqg7d   helloworld.1   alpine:latest   i03       Running         Running 6 minutes ago              
77ffubnsfwns   helloworld.2   alpine:latest   i04       Running         Running 33 seconds ago             
wdd938a2k5ps   helloworld.3   alpine:latest   i03       Running         Running 36 seconds ago             
4s00ydifx0d0   helloworld.4   alpine:latest   i04       Running         Running 33 seconds ago             
koazyetimren   helloworld.5   alpine:latest   i04       Running         Running 33 seconds ago
```

## DELETE THE SERVICE

```
$ docker service rm helloworld -> per esborrar els serveis
helloworld

$ docker service ls -> podem veure que no ens llista cap servei, degut a que l'hem borrat. 
ID        NAME      MODE      REPLICAS   IMAGE     PORTS

$ docker ps -> no hi ha cap contenidor encès.
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

## ROLLING UPDATES

```
$ docker service create --replicas 3 --name redis --update-delay 10s redis:3.0.6 -> creem un servei anomenat redis, que tingui tres rèpliques i diem que té un retard dactualització de 10 segons, per últim diem que la imatge serà "redis:3.0.6"

a9e0s47tpxvl19g14eg5bgy05
overall progress: 3 out of 3 tasks 
1/3: running   
2/3: running   
3/3: running   
verify: Service converged 

------------

$ docker service ps redis -> si llistem podem veure que tenim tres rèpliques: 2 en el i04 i 1 en el i03 (MANAGER)
ID             NAME      IMAGE         NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
inz4td48jw4v   redis.1   redis:3.0.6   i04       Running         Running 45 seconds ago             
qwyyhatwejzw   redis.2   redis:3.0.6   i04       Running         Running 45 seconds ago             
rm9yru31oqau   redis.3   redis:3.0.6   i03       Running         Running 46 seconds ago   

------------

$ docker service inspect --pretty redis -> dona una informació llegible i fàcil d'entrendre de la imatge redis

ID:		a9e0s47tpxvl19g14eg5bgy05
Name:		redis
Service Mode:	Replicated
 Replicas:	3
Placement:
UpdateConfig:
 Parallelism:	1
 Delay:		10s
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Update order:      stop-first
RollbackConfig:
 Parallelism:	1
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Rollback order:    stop-first
ContainerSpec:
 Image:		redis:3.0.6@sha256:6a692a76c2081888b589e26e6ec835743119fe453d67ecf03df7de5b73d69842
 Init:		false
Resources:
Endpoint Mode:	vip
```

Podem veure de que a l'ordinador WORKER s'ha encès un contenidor, encara que no hem fet cap ordre, això succeix gràcies al "docker swarm". 

```
$ docker ps -> ordinador WORKER  
CONTAINER ID   IMAGE     	COMMAND              	CREATED     	STATUS      	PORTS                                              	NAMES
a2466bc13e36   redis:3.0.7   "docker-entrypoint.s…"   3 minutes ago   Up 3 minutes	6379/tcp                                           	redis.2.j4n4pyvoup252cmqv25q5u275
```

## CREATE SERVER GNIX

**En el ordinador del MANAGER creem un servidor nginx, aquest es propagarà pel port 8080.**

```
$docker service create --name my-web --publish published=8080,target=80 --replicas 2 nginx

ufgrkws5vy17gobp11scenw0r
overall progress: 2 out of 2 tasks 
1/2: running   [==================================================>] 
2/2: running   [==================================================>] 
verify: Service converged 

ORDINADOR DE MANAGER:
http://localhost:8080

ORDINADOR DE WORKER
http://localhost:8080

Tots dos poden veure la mateixa pantalla (el servidor nginx)
```
