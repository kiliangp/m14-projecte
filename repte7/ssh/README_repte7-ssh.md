# M14-REPTE7-NET. CREACIÓ DE SERVIDORS: SSH, SAMBA I NET

Crear els servidors següents:
* Net: Implementa els serveis de xarxa echo, daytime, chargen, ftp, tftp i http.
* SAMBA: implementa un servidor samba stand alone.
* SSH amb autenticació contra el servidor ldap.

## DOCKERFILE
```
FROM debian
LABEL author="Kilian Steven Guanoluisa Pionce"
LABEL subject="Repte-7-ssh"
RUN apt-get update
RUN apt-get install -y openssh-client openssh-server procps iproute2
COPY useradd.sh /opt/docker/
RUN bash /opt/docker/useradd.sh
WORKDIR /opt/docker
CMD /etc/init.d/ssh start -D
EXPOSE 22
```
## USERADD.SH 
```
#! /bin/bash

# Creació dels usuaris per verificat accés SSH
for user in unix1 unix2 unix3 unix4 unix5
do
	useradd -m $user
	echo $user:$user | chpasswd
done
```

# ORDRES
```
docker build -t kiliangp/repte7-ssh:latest .
```
## SSH LOCALMENT
```
docker run --rm --name ssh -h ssh.edt.org -p 22 -d kiliangp/repte7-ssh
docker exec -it ssh /bin/bash

root@ssh:/opt/docker# ssh unix1@localhost 
    The authenticity of host 'localhost (127.0.0.1)' can't be established.
    ED25519 key fingerprint is SHA256:R9yx3QmbIVMh+76r2Sa9pmhfIQARWwlUb2iu27S/4bc.
    This key is not known by any other names.
    Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
    Warning: Permanently added 'localhost' (ED25519) to the list of known hosts.
    unix1@localhost's password: 
    Linux ssh.edt.org 6.1.0-0.deb11.11-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.38-4~bpo11+1 (2023-08-08) x86_64

    The programs included with the Debian GNU/Linux system are free software;
    the exact distribution terms for each program are described in the
    individual files in /usr/share/doc/*/copyright.

    Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
    permitted by applicable law.
    $

Podem veure la ip amb un "ip a" per connectar-nos desde el client:
root@ssh:/opt/docker# ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
        inet 127.0.0.1/8 scope host lo
           valid_lft forever preferred_lft forever
    47: eth0@if48: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
        link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
        inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
           valid_lft forever preferred_lft forever
```

## SSH CLIENT
```
docker run --rm --name client -h client.edt.org -p 22 -it kiliangp/repte7-ssh /bin/bash
root@client:/opt/docker# ssh unix1@172.17.0.2
    unix1@172.17.0.2's password:        -> El passwd serà el mateix que l'usuari
    Linux ssh.edt.org 6.1.0-0.deb11.11-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.38-4~bpo11+1 (2023-08-08) x86_64

    The programs included with the Debian GNU/Linux system are free software;
    the exact distribution terms for each program are described in the
    individual files in /usr/share/doc/*/copyright.

    Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
    permitted by applicable law.
    Last login: Thu Oct 26 16:22:50 2023 from 127.0.0.1
    $ 
```
