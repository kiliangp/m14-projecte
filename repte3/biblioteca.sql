CREATE DATABASE biblioteca;
\c biblioteca; 

CREATE TABLE llibres (
    id INT AUTO_INCREMENT PRIMARY KEY,
    titol VARCHAR(255) NOT NULL,
    autor VARCHAR(255),
    any_publicacio INT
);

INSERT INTO llibres (titol, autor, any_publicacio) VALUES
    ('El Hobbit', 'J.R.R. Tolkien', 1937),
    ('1984', 'George Orwell', 1949),
    ('Cien años de soledad', 'Gabriel García Márquez', 1967),
    ('Harry Potter y la piedra filosofal', 'J.K. Rowling', 1997);


