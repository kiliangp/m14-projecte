# M14-REPTE8. DOCKER COMPOSE: iniciació

Implementar els següents exemples de docker-compose:
* Exemple web + portainer
* Exemple net + portainer
* Exemple web + net + portainer
* Exemple postgres + adminer
* Exemple ldap + phpldapadmin

## DOCKER-COMPOSE.YML
```
version: "2"
services:
  web:
    image: kiliangp/web23:base
    container_name: web.edt.org
    hostname: web.edt.org
    ports:
      - "80:80"
    networks:
      - mynet
  portainer:
    image: portainer/portainer
    ports:
      - "9000:9000"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
    - mynet
networks:
  mynet:

```
* Utilitzem el fitxer **yaml** per fer el "docker-compose amb la següent ordre (l'engeguem):
```
docker compose -f docker-compose.yml up -d
```
<br>
Per apagar les dues imatges:

```
docker compose down
```

## COMPROVACIONS

1. **PRIMERA IMATGE**

```
En el navegador:
  http://localhost
    - Sortirà la pàgina web creada
```
2. **SEGONA IMATGE**

```
En el navegador:
  http://localhost:9000
    - Sortirà la pàgina web de portainer
```

