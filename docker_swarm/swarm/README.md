# M14-REPTE3. GET STARTED WITH SWARM
* Implementar els exercicis i exemples per fer desplegaments en varis nodes utilitzant docker swarm i docker nodes.
* Observar el desplegament utilitzant un container visualizer.

```
$ docker swarm init -> desde el meu ordinador (i03)
Swarm initialized: current node (n521ysoji9o9tpk3l8ggi5d4w) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-554xzefqaewv4b8meczn33zpb78v8rh9dskgolyb863biuqs30-apwpxf6itnls0o5t51gjdueek 10.200.243.203:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.


$ docker swarm join --token SWMTKN-1-554xzefqaewv4b8meczn33zpb78v8rh9dskgolyb863biuqs30-apwpxf6itnls0o5t51gjdueek 10.200.243.203:2377
    - Ordre que fem a l'ordinador WORKER, en aquest cas, l'ordinador i04


$ docker node ls -> en el ordinador Manager podem veure que l'ordinador i04 està connectat com a WORKER.
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
n521ysoji9o9tpk3l8ggi5d4w *   i03        Ready     Active         Leader           24.0.7
xe34sy4hqcwd38q10dy91ielh     i04        Ready     Active                          24.0.2
```

```
$ docker stack deploy -c docker-compose.yml myapp -> Desde Manager
Creating network myapp_webnet
Creating service myapp_redis
Creating service myapp_visualizer
Creating service myapp_portainer
Creating service myapp_web

$ docker ps -> veiem els contenidors que ens ha encès.
CONTAINER ID   IMAGE                             COMMAND                  CREATED          STATUS          PORTS                          NAMES
47f1c6e80c9e   kiliangp/getstarted:comptador     "python app.py"          46 seconds ago   Up 43 seconds   80/tcp                         myapp_web.1.bzlihkld8m7gambckcyhjrji3
7a91f525cbc9   redis:latest                      "docker-entrypoint.s…"   50 seconds ago   Up 48 seconds   6379/tcp                       myapp_redis.1.j8v7aea4qwytvrtm9rw3fh6e8
575ce6587847   portainer/portainer:latest        "/portainer"             51 seconds ago   Up 49 seconds   8000/tcp, 9000/tcp, 9443/tcp   myapp_portainer.1.rziy7wn8gjsdcc5c381i4yotq
3e84bfa563c8   dockersamples/visualizer:stable   "npm start"              52 seconds ago   Up 51 seconds   8080/tcp                       myapp_visualizer.1.weblqk5zbikiwwn0n26cy0heh

```

## ORDRE AL WORKER

```
$ docker ps -> podem veure que s'ha encès el contenidor a partir del docker stack
CONTAINER ID   IMAGE                       	COMMAND              	CREATED     	STATUS      	PORTS                                              	NAMES
9183dadb75c4   kiliangp/getstarted:comptador   "python app.py"      	4 minutes ago   Up 4 minutes	80/ tcp                                             	myapp_web.2.ymx5h3wkugdkmybyo6lx6xjlj

http://localhost -> si recarguem, el número de visites augmentarà, el que passarà es que si recarguem la pàgina de l'ordinador de MANAGER, veurà la següent recarga.
```

## DOCKER NODE

```
docker node update -> especifíca que s'està actualitzant la configuració d'un node en el clúster.
--availability -> s'indica que s'està canviant l'estat de disponibilitat del node.

    * pause: posa el node en pausa
    * drain: posa el node en mode drenatge, això significa que no es programaràn nous contenidors en aquest node, pero els contenidors en execució continuaràn en el fins que terminin.
    * active: posa el node un altre vegada a funcionar.    

docker node update --availability pause i04
docker node update --availability drain i04
docker node update --availability active i04
```

Podem posar etiquetes dintre del node a partir d'aquestes comandes.

```
docker node update --label-add pais=espanya i04

docker node inspect i04 -> podem veure el principi que s'ha modificat l'etiqueta.

  placement:
    constraints: [node.labels.pais == espanya ]

  placement:
    constraints: [node.role == manager] -> el que especifiquem en aquesta etiqueta és que només ho podrà fer manager
```
