# M14-REPTE 3. UTILITZACIÓ DE VOLUMS EN UN SERVIDOR POSTGRES
* **V1**: Imatge que utilitza la base de dades postgres amb la utilització de volums i persistència de dades.
Execució:
1. **docker volume create repte3**
* Creem el volum el qual l'anomenarem "repte3"

2. **docker volume ls**
* Comprovem si el volum s'ha creat correctament.

3. **vim biblioteca.sql**
* Creem una BD en el que introduïm dades per tenir aquesta persistència que busquem.

4. **vim Dockerfile**
* Dintre d'aquest Dockerfile el que farem és instal·lar la imatge "postgres", a més de posar l'ordre següent:
* **COPY biblioteca.sql /docker-entrypoint-initdb.d/** -> El que diem és que copiï la nostra base de dades i que la introdueixi dintre d'aquest directori de docker, aquest és el que farà que la base de dades estigui como a principal a l'hora de posar en marxa la imatge..

5. **docker build -t repte3:latest .**
*  Creem la imatge a partir del Dockerfile

6. **docker run --rm --name repte3-postgres -e POSTGRES_PASSWORD=passwd -e POSTGRES=DB=training -v postgres-dades:/var/lib/postgresql/data -d repte3:latest**
* Fem que el container estigui dintre del volum, posem un nombre al container per a poder identificar-ho, li posem una contrasenya al postgres i finalment l'iniciem en detach.

7. **docker exec -it repte3 psql -U postgres -d training -c "SELECT * FROM llibres**

8. **\c biblioteca**
* Entrem a la nostra base de dades.

9. **SELECT * FROM Autors;**
* Fem una prova per veure si hi ha registres. 

10. **CREATE TABLE usuarios (
    id INT PRIMARY KEY,
    nombre VARCHAR(50),
    edad INT,
    correo VARCHAR(100)
);**

11. **Control + D**
* Sortim de la sessió del container

12. **docker exec -it repte3-postgress psql -U postgres**
* L'iniciem per veure si hi ha persistència de dades.

13. **\c biblioteca**

14. **\d**
* Podem veure les taules que hi ha, efectivament la taula està creada, per tant hi ha persistència

