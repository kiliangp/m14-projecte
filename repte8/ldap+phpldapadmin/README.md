# M14-REPTE8. DOCKER COMPOSE: iniciació

Implementar els següents exemples de docker-compose:
* Exemple web + portainer
* Exemple net + portainer
* Exemple web + net + portainer
* Exemple postgres + adminer
* Exemple ldap + phpldapadmin

## DOCKER-COMPOSE.YML
```
version: "3"
services:
  ldap:
    image: kiliangp/ldap2023:base
    container_name: ldapserver
    hostname: ldapserver
    ports:
      - "389:389"
    networks:
      - mynet
  phpldapadmin:
    image: kiliangp/phpldapadmin:latest
    container_name: phpldap
    hostname: phpldap
    ports:
      - "80:80"
    networks:
      - mynet
networks:
  mynet:
```
* Utilitzem el fitxer **yaml** per fer el "docker-compose amb la següent ordre (l'engeguem):
```
docker compose -f docker-compose.yml up -d
```
<br>
Per apagar les dues imatges:

```
docker compose down
```

## COMPROVACIONS

1. http://localhost/phpldapadmin
* login DN -> cn=Manager,dc=edt,dc=org 
* Password -> secret
2. **També podem fer búsquedes desde un usuari, per exemple**
* login DN -> cn=Anna Pou,ou=usuaris,dc=edt,dc=org
* Password -> anna


  
