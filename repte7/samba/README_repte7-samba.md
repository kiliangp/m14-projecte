# M14-REPTE7-NET. CREACIÓ DE SERVIDORS: SSH, SAMBA I NET

Crear els servidors següents:
* Net: Implementa els serveis de xarxa echo, daytime, chargen, ftp, tftp i http.
* SAMBA: implementa un servidor samba stand alone. Consisteix en crear un servidor Samba-3 Stand Alone que comparteixi recursos de disc (File Shares) on s’hi poden connectar tant clients GNU/Linux com clients
Windows.
* SSH amb autenticació contra el servidor ldap.

## DOCKERFILE
```
FROM debian:latest
LABEL author="Kilian Steven Guanoluisa Pionce"
LABEL subject="Repte7-samba"
RUN apt-get update
RUN apt-get install -y samba smbclient
RUN mkdir /opt/docker
COPY * /opt/docker
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 139 445
```

## STARTUP.SH 
```
#! /bin/bash
# PUBLIC
#######################
mkdir /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

#######################
# CONFIGURACIÓ DEL SAMBA
cp /opt/docker/smb.conf /etc/samba/smb.conf

# Creació d'usuari per el samba 
useradd -m -s /bin/bash kilian
echo -e "kilian\nkilian" |smbpasswd -a kilian

# Activem els serveis
/usr/sbin/smbd 
/usr/sbin/nmbd -F 
```

## SMB.CONF
```
[public]
comment = Contingut pubic
path = /var/lib/samba/public
public = yes
browseable = yes
writable = yes
printable = no
guest ok = yes
#####################################
[privat]
comment = Contingut privat
path = /var/lib/samba/privat
public = no
browseable = no
writable = yes
printable = no
guest ok = yes

```

## ORDRES
```
docker build -t kiliangp/repte7-samba:latest .
docker run --rm --name samba -p 139:139 -p 445:445 -d kilian/repte7-samba:latest
docker exec -it samba /bin/bash
```
```
Dintre del container:

root@samba_:/opt/docker# smbclient //localhost/public -U kilian
Password for [WORKGROUP\kilian]:
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Sat Oct 28 09:00:11 2023
  ..                                  D        0  Sat Oct 28 09:00:12 2023
  Dockerfile                          N      294  Sat Oct 28 09:00:11 2023
  startup.sh                          N      770  Sat Oct 28 09:00:11 2023
  smb.conf                            N      316  Sat Oct 28 09:00:11 2023

		401068360 blocks of size 1024. 311681420 blocks available
smb: \> 


----------------
Fora del container:
smbclient //172.17.0.2/public -U kilian
Enter WORKGROUP\kilian's password: 
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Sat Oct 28 11:00:11 2023
  ..                                  D        0  Sat Oct 28 11:00:12 2023
  Dockerfile                          N      294  Sat Oct 28 11:00:11 2023
  startup.sh                          N      770  Sat Oct 28 11:00:11 2023
  smb.conf                            N      316  Sat Oct 28 11:00:11 2023

		401068360 blocks of size 1024. 311681388 blocks available
smb: \> 

```
