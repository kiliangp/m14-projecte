# Container amb servidor web Apache2 que funcioni en detach
Repte-2.

Imatge:
* **Versió 1**. Incorpora els paquets nmap, tree, vim, procps, iproute2, apache2 .

### Execució:
1. **sudo usermod -aG docker guest**
* Associem el grup docker a l'usuari guest perquè no ens surti cap error i tingui els permisos necessaris.
2. **sudo newgrp**
* Fem aquesta ordre perquè l'usuari guest entri de manera temporal al grup de Docker.
3. **vim Dockerfile**
* Creem un Dockerfile amb l'objectiu d'introduir totes les comandes que farà el container amb totes les actualitzacions prèvies.
4. **vim startup.sh**
* Dintre d'aquest fitxer activem el servei d'apache2 amb la finalitat d'obrir el port 80.
#### **Dintre del Dockerfile**
1. **FROM debian:latest**
* Indiquem quin debian estem utilitzant
2. **LABEL author="@edt Kilian ASIX repte_2"**
* Indiquem l'autor d'aquest Docker
3. **RUN apt-get update**
* Ordre que farà automàticament una vegada s'inicialitzi el container, per actualitzar els paquets desitjats.
4. **RUN apt-get install -y procps nmap tree vim apache2 iproute2**
* Instalem els paquets per a la pràctica.
5. **COPY index.html /var/www/html/**
* Copiem el fitxer del nostre directori amb l'objectiu d'introduir-ho dintre del container, aquest contindrà una pàgina web, l'utilitzem per veure que el servidor apache2 funciona correctament
6. **COPY startup.sh /tmp**
7. **CMD /tmp/startup.sh**
8. **EXPOSE 80**
9. **CMD apachectl -k start -X**
* Li estem dient que ho fasi en foreground(k) i que a més ho deixi encesa
#### **Fora del Dockerfile**
1. **docker build -t kiliangp/container_repte2**
* Creem una imatge a partir del container.

2. **docker run --rm --name repte2 -p 80:80 -d kiliangp/container_repte2** 
* Creem un container amb propagació de port 80 amb l'imatge que ja hem creat.

3. **docker exec -it repte2 /bin/bash**

4. **docker tag kiliangp/container_repte2 kiliangp/container_detach:detach**
* Canviem el nom per identificar-ho en el repositori de DockerHub

5. **docker push kiliangp/container_detach:detach**
* Pugem la imatge al nostre repositori Docker

