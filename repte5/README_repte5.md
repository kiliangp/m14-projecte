# M14-SERVIDOR LDAP AMB ENTRYPOINT (CONFIGURABLE)

Imatge que s'executa amb un entrypoint que és un script que actuarà segons li indiqui l'argument rebut. 

## Dockerfile

```
# Repte5
FROM debian:latest
LABEL version="1.0"
LABEL author="Kilian Steven Guanoluisa Pionce"
LABEL subject="Repte5"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install procps iproute2 tree nmap vim ldap-utils slapd
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
ENTRYPOINT [ "/bin/bash" , "/opt/docker/startup.sh" ]
WORKDIR /opt/docker
EXPOSE 389

```
**DEBIAN_FRONTEND=noninteractive** -> define una variable de entorno que se pasará del directorio de entorno dentro del container, significa que la instalación de paquetes Debian no sea interactivo, por ejemplo, el paquete de ldap te hacía preguntas

## startup.sh
```
#! /bin/bash

function initdb(){
 rm -rf /etc/ldap/slapd.d/*
 rm -rf /var/lib/ldap/*
 slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
 slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
 chown -R openldap:openldap /etc/ldap/slapd.d
 chown -R openldap:openldap /var/lib/ldap
 /usr/sbin/slapd -d0
}

function slapd(){
 rm -rf /etc/ldap/slapd.d/*
 rm -rf /var/lib/ldap/*
 slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
 chown -R openldap:openldap /etc/ldap/slapd.d
 chown -R openldap:openldap /var/lib/ldap
 /usr/sbin/slapd -d0
}

function edt(){
 /usr/sbin/slapd -d0

}
# Creem una variable en cas de que hi hagi un espai en blanc (no hi hagin arguments)
if [ "$#" -eq 0 ]; then
	res="start"
else
	res="$1"
fi

case "$res" in
 "initdb")
  echo "Ho inicialitza tot de nou i fa el populate de edt.org" 
  initdb
  ;;
 "slapd")
  echo "Ho inicialitza tot però només engega el servidor, sense posar-hi dades" 
  slapd
  ;;
 "start"| "edtorg")
  echo "Engega el servidor utilitzant la persistència de dades de la bd i de la configuració. És a dir, engega el servei usant les dades ja existents"
  edt
  
  ;;
 "slapcat")
    if [ "$2" -eq 0 -o -eq 1 ]; then
	    slapcat -n "$2"
    elif [ -z "$2" ]; then
	    slapcat
    fi
# slapcat 0 -> fa un slapcat de la base de dades de la configuració del dimoni
# slapcat | slapcat 1 -> fa un slapcat de la primera base de dades (default)
    # En cas de que hi hagi un argument (0,1) en el $2 executem el primer "if", en cas contrari entrarem en l'elif on engloba que "si està buit, es cunpleix", en aquest, fem un slapcat.
    ;;
*)
	exit 1
        ;;

esac

```

## edt-org.ldif

```
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org

dn: ou=maquines,dc=edt,dc=org
ou: maquines
description: Container per a maquines linux
objectclass: organizationalunit

dn: ou=clients,dc=edt,dc=org
ou: clients
description: Container per a clients linux
objectclass: organizationalunit

dn: ou=productes,dc=edt,dc=org
ou: productes
description: Container per a productes linux
objectclass: organizationalunit

dn: ou=usuaris,dc=edt,dc=org
ou: usuaris
description: Container per usuaris del sistema linux
objectclass: organizationalunit

dn: cn=Pau Pou,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pau Pou
cn: Pauet Pou
sn: Pou
homephone: 555-222-2220
mail: pau@edt.org
description: Watch out for this guy
ou: Profes
uid: pau
uidNumber: 5000
gidNumber: 100
homeDirectory: /tmp/home/pau
userPassword: {SSHA}NDkipesNQqTFDgGJfyraLz/csZAIlk2/

dn: cn=Pere Pou,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pere Pou
sn: Pou
homephone: 555-222-2221
mail: pere@edt.org
description: Watch out for this guy
ou: Profes
uid: pere
uidNumber: 5001
gidNumber: 100
homeDirectory: /tmp/home/pere
userPassword: {SSHA}ghmtRL11YtXoUhIP7z6f7nb8RCNadFe+

dn: cn=Anna Pou,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Anna Pou
cn: Anita Pou
sn: Pou
homephone: 555-222-2222
mail: anna@edt.org
description: Watch out for this girl
ou: Alumnes
uid: anna
uidNumber: 5002
gidNumber: 600
homeDirectory: /tmp/home/anna
userPassword: {SSHA}Bm4B3Bu/fuH6Bby9lgxfFAwLYrK0RbOq

dn: cn=Marta Mas,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Marta Mas
sn: Mas
homephone: 555-222-2223
mail: marta@edt.org
description: Watch out for this girl
ou: Alumnes
uid: marta
uidNumber: 5003
gidNumber: 600
homeDirectory: /tmp/home/marta
userPassword: {SSHA}9+1F2f5vcW8z/tmSzYNWdlz5GbDCyoOw

dn: cn=Jordi Mas,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Jordi Mas
cn: Giorgios Mas
sn: Mas
homephone: 555-222-2224
mail: jordi@edt.org
description: Watch out for this girl
ou: Alumnes
ou: Profes
uid: jordi
uidNumber: 5004
gidNumber: 100
homeDirectory: /tmp/home/jordi
userPassword: {SSHA}T5jrMgpJwZZgu0azkLIVoYhiG08/KGUv

dn: cn=Admin System,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Administrador Sistema
cn: System Admin
sn: System
homephone: 555-222-2225
mail: anna@edt.org
description: Watch out for this girl
ou: system
ou: admin
uid: admin
uidNumber: 10
gidNumber: 10
homeDirectory: /tmp/home/admin
userPassword: {SSHA}4mS0FycWc5bkpW8/a396SGNDTQUlFSX3

```

## slapd.conf

```
#
# Veure slapd.conf(5) per detalls sobre les opcions de configuració.
# Aquest arxiu NO hauria de ser llegible per a tothom.
#
# Paquets Debian: slapd ldap-utils

include         /etc/ldap/schema/corba.schema
include         /etc/ldap/schema/core.schema
include         /etc/ldap/schema/cosine.schema
include         /etc/ldap/schema/duaconf.schema
include         /etc/ldap/schema/dyngroup.schema
include         /etc/ldap/schema/inetorgperson.schema
include         /etc/ldap/schema/java.schema
include         /etc/ldap/schema/misc.schema
include         /etc/ldap/schema/nis.schema
include         /etc/ldap/schema/openldap.schema
#include                /etc/ldap/schema/ppolicy.schema
include         /etc/ldap/schema/collective.schema

# Permetre connexions de clients LDAPv2. Això NO és la configuració per defecte.
allow bind_v2

pidfile         /var/run/slapd/slapd.pid
#argsfile       /var/run/openldap/slapd.args

modulepath /usr/lib/ldap
moduleload back_mdb.la
moduleload back_monitor.la
# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
rootpw secret
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
# ----------------------------------------------------------------------
database monitor
access to *
       by dn.exact="cn=Manager,dc=edt,dc=org" read
       by * none

```
## OBJECTIU

* Canviar al Dockerfile el CMD per ENTRYPOINT
* Executa l’script startup, però rep els arguments:

        initdb → ho inicialitza tot de nou i fa el populate de edt.org

        slapd → ho inicialitza tot però només engega el servidor, sense posar-hi dades

        start / edtorg / res → engega el servidor utilitzant la persistència de dades de la bd i de la configuració. És a dir, engega el servei usant les dades ja existents

        slapcat nº (0,1, res) → fa un slapcat de la base de dades indicada 

## Ordres

Creem l'imatge
```
docker build -t kiliangp/repte5:base .
```

initdb:
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -v repte5-config:/etc/ldap/slapd.d/ -v repte5-data:/var/lib/ldap/ -d kiliangp/repte5:base initdb
```
slapd:
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -v repte5-config:/etc/ldap/slapd.d/ -v repte5-data:/var/lib/ldap/ -d kiliangp/repte5:base slapd
```
start|edtorg|res:
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -v repte5-config:/etc/ldap/slapd.d/ -v repte5-data:/var/lib/ldap/ -d kiliangp/repte5:base start

docker run --rm --name ldap.edt.org -h ldap.edt.org -v repte5-config:/etc/ldap/slapd.d/ -v repte5-data:/var/lib/ldap/ -d kiliangp/repte5:base edtorg

docker run --rm --name ldap.edt.org -h ldap.edt.org -v repte5-config:/etc/ldap/slapd.d/ -v repte5-data:/var/lib/ldap/ -d kiliangp/repte5:base  

```
Si volem veure si hi ha persistència de dades el que podem fer és crear un fitxer "vim kill.ldif", en el que podem eliminar dos usuaris a partir d'aquesta ordre:
* **ldapdelete -x -D 'cn=Manager,dc=edt,dc=org' -w secret -f kill.ldif**

Per comprobar si hem eliminat correctament els usuaris fem la següent ordre:
* **ldapsearch -x -LLL -b 'dc=edt,dc=org' dn**

Hem de sortir del container, eliminar la imatge i tornar a encendre per veure si el mvolum funciona correctament a partir d'aquesta ordre:

```
docker run --rm --name ldap.edt.org -h ldap.edt.org -v repte5-config:/etc/ldap/slapd.d/ -v repte5-data:/var/lib/ldap/ -d kiliangp/repte5:base start
```
Després d'aquesta ordre farem:
```
docker exec -it ldap.edt.org /bin/bash
ldapsearch -x -LLL -b 'dc=edt,dc=org' dn
```
slapcat nº (0,1,res)
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -v repte5-config:/etc/ldap/slapd.d/ -v repte5-data:/var/lib/ldap/ -it kiliangp/repte5:base slapcat 
docker run --rm --name ldap.edt.org -h ldap.edt.org -v repte5-config:/etc/ldap/slapd.d/ -v repte5-data:/var/lib/ldap/ -it kiliangp/repte5:base slapcat 0
docker run --rm --name ldap.edt.org -h ldap.edt.org -v repte5-config:/etc/ldap/slapd.d/ -v repte5-data:/var/lib/ldap/ -it kiliangp/repte5:base slapcat 1



```
