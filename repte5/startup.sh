#! /bin/bash
# Kilian Steven Guanoluisa Pionce

function initdb(){
 rm -rf /etc/ldap/slapd.d/*
 rm -rf /var/lib/ldap/*
 slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
 slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
 chown -R openldap.openldap /etc/ldap/slapd.d
 chown -R openldap.openldap /var/lib/ldap
 /usr/sbin/slapd -d0
}

function slapd(){
 rm -rf /etc/ldap/slapd.d/*
 rm -rf /var/lib/ldap/*
 slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
 chown -R openldap.openldap /etc/ldap/slapd.d
 chown -R openldap.openldap /var/lib/ldap
 /usr/sbin/slapd -d0
}

function edt(){
 /usr/sbin/slapd -d0

}

if [ "$#" -eq 0 ]; then
	res="start"
else
	res="$1"
fi

case "$res" in
 "initdb")
  echo "Ho inicialitza tot de nou i fa el populate de edt.org" 
  initdb
  ;;
 "slapd")
  echo "Ho inicialitza tot però només engega el servidor, sense posar-hi dades" 
  slapd
  ;;
 "start"| "edtorg")
  echo "Engega el servidor utilitzant la persistència de dades de la bd i de la configuració. És a dir, engega el servei usant les dades ja existents"
  edt
  
  ;;
 "slapcat")
    if [ "$2" -eq 0 -o -eq 1 ]; then
	    slapcat -n "$2"
    elif [ -z "$2" ]; then
	    slapcat
    fi
    ;;
*)
	exit 1
        ;;

esac
