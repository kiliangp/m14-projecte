# M14-REPTE10. RÈPLIQUES AMB DOCKER COMPOSE
* Implementar l'exemple de rèpliques utilitzant ports dinàmics.
## DOCKER-COMPOSE.YML
```
version: "3"
services:
  web:
    image: kiliangp/getstarted:comptador
    ports:
      - "80"
    networks:
      - webnet
  web2:
    image: kiliangp/web23:base
    ports:
      - "80"
    networks:
      - webnet
  redis:
    image: redis
    ports:
      - "6379:6379"
    command: redis-server --appendonly yes
    networks:
      - webnet
  visualizer:
    image: dockersamples/visualizer:stable
    ports:
      - "8080:8080"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
      - webnet
  portainer:
    image: portainer/portainer
    ports:
      - "9000:9000"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
      - webnet
networks:
  webnet:
```


<br>
El que hem fet és posar-li un volum perquè hi hagi persitència de dades, quan apaguem el contenidor y l'engeguem una altre vegada, es guardarà el número de visites que teníem.

* Utilitzem el fitxer **yaml** per fer el "docker-compose amb la següent ordre (l'engeguem):
```
docker compose -f docker-compose.yml up -d
```
<br>
Per apagar les imatges:

```
docker compose down
```

## COMPROVACIONS
1. Per veure que ports utilitzem en els nostres contenidors:
```
docker compose ps

NAME                  IMAGE                             COMMAND                                                SERVICE      CREATED          STATUS          PORTS
reptex-portainer-1    portainer/portainer               "/portainer"                                           portainer    10 minutes ago   Up 10 minutes   8000/tcp, 9443/tcp, 0.0.0.0:9000->9000/tcp, :::9000->9000/tcp
reptex-redis-1        redis                             "docker-entrypoint.sh redis-server --appendonly yes"   redis        10 minutes ago   Up 10 minutes   0.0.0.0:6379->6379/tcp, :::6379->6379/tcp
reptex-visualizer-1   dockersamples/visualizer:stable   "npm start"                                            visualizer   10 minutes ago   Up 10 minutes   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp
reptex-web-1          edtasixm05/getstarted:comptador   "python app.py"                                        web          10 minutes ago   Up 10 minutes   0.0.0.0:32772->80/tcp, :::32772->80/tcp
reptex-web2-1         kiliangp/web23:base               "/bin/sh -c /opt/docker/startup.sh"                    web2         10 minutes ago   Up 10 minutes   0.0.0.0:32773->80/tcp, :::32773->80/tcp
```
2. Per veure el servei de portainer -> http://localhost:9000
3. Per veure el servei de redis -> El que podrem fer és eliminar la imatge i tornar-la a encendre, el que conseguim amb això és veure si hi ha persistència de dades. 
4. Per veure el servei de visualizer -> http://localhost:8080 
5. Per veure el servei de comptador -> http://localhost:32772
6. Per veure el servei de web -> http://localhost:32773

-- 

Podem desplegar un número de rèpliques personsalitzades amb la següent ordre:
```
docker compose up -d --scale web=4


docker compose ps

NAME                  IMAGE                             COMMAND                                                SERVICE      CREATED         STATUS         PORTS
reptex-portainer-1    portainer/portainer               "/portainer"                                           portainer    3 minutes ago   Up 3 minutes   8000/tcp, 9443/tcp, 0.0.0.0:9000->9000/tcp, :::9000->9000/tcp
reptex-redis-1        redis                             "docker-entrypoint.sh redis-server --appendonly yes"   redis        3 minutes ago   Up 3 minutes   0.0.0.0:6379->6379/tcp, :::6379->6379/tcp
reptex-visualizer-1   dockersamples/visualizer:stable   "npm start"                                            visualizer   3 minutes ago   Up 3 minutes   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp
reptex-web-1          edtasixm05/getstarted:comptador   "python app.py"                                        web          3 minutes ago   Up 3 minutes   0.0.0.0:32774->80/tcp, :::32774->80/tcp
reptex-web-2          edtasixm05/getstarted:comptador   "python app.py"                                        web          6 seconds ago   Up 4 seconds   0.0.0.0:32777->80/tcp, :::32777->80/tcp
reptex-web-3          edtasixm05/getstarted:comptador   "python app.py"                                        web          6 seconds ago   Up 5 seconds   0.0.0.0:32776->80/tcp, :::32776->80/tcp
reptex-web-4          edtasixm05/getstarted:comptador   "python app.py"                                        web          6 seconds ago   Up 4 seconds   0.0.0.0:32778->80/tcp, :::32778->80/tcp
reptex-web2-1         kiliangp/web23:base               "/bin/sh -c /opt/docker/startup.sh"                    web2         3 minutes ago   Up 3 minutes   0.0.0.0:32775->80/tcp, :::32775->80/tcp

```
Podem veure que indicant el nom del servei que volem replicar, podem tenir les rèpliques que volguem
