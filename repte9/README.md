# M14-REPTE9. COMPTADOR DE VISITES
* Implementar l'exemple de la documentació del comptador de visites.
## DOCKERFILE
```
FROM python:3.7-alpine
LABEL author="Kilian Steven Guanoluisa Pionce"
LABEL subject="Repte9"
WORKDIR /code
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000
COPY . .
CMD ["flask", "run"]
```

## APP.PY
```
import time

import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

@app.route('/')
def hello():
    count = get_hit_count()
    return 'Has visitat la meva pàgina web {} vegades.\n'.format(count)
```

## REQUIREMENTS.TXT
```
flask
redis
```
## DOCKER-COMPOSE.YML
```
services:
  web:
    build: .
    ports:
      - "5000:5000"
    volumes:
      - .:/code
    environment:
      FLASK_ENV: development
  redis:
    image: "redis:alpine"
    container_name: redis-container
    ports:
      - "6379:6379"
    volumes:
      - "./data:/data"
    command: redis-server --appendonly yes
```

<br>
El que hem fet és posar-li un volum perquè hi hagi persitència de dades, quan apaguem el contenidor y l'engeguem una altre vegada, es guardarà el número de visites que teníem.

* Utilitzem el fitxer **yaml** per fer el "docker-compose amb la següent ordre (l'engeguem):
```
docker compose -f docker-compose.yml up -d
```
<br>
Per apagar les dues imatges:

```
docker compose down
```

## COMPROVACIONS
1. http://localhost:5000
2. Si recarguem la pàgina, el número de visites augmenta
