# M14-REPTE2. DEPLOY
* Implementar els exercicis i exemples per fer deploy de containers usant docker stack i service. 

## DOCKER-COMPOSE.YML 

**Fitxer docker compose que utilitzarem per fer les proves amb l'ordre "docker swarm service scale"**.

```
version: "3"
services:
  web:
    image: kiliangp/getstarted:comptador 
    deploy:
      replicas: 2
      restart_policy:
        condition: on-failure
      resources:
        limits:
          cpus: "0.1"
          memory: 50M
    ports:
      - "80:80"
    networks:
      - webnet
  redis:
    image: redis
    ports:
      - "6379:6379"
    deploy:
      placement:
        constraints: [node.role == manager] -> definim restriccions que només podrà utilitzar MANAGER, per tant, enacara que indiquem moltes rèpliques, només es mostraran a l'ordinador de MANAGER.
    command: redis-server --appendonly yes
    networks:
      - webnet
  visualizer:
    image: dockersamples/visualizer:stable
    ports:
      - "8080:8080"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    deploy:
      placement:
        constraints: [node.role == manager]
    networks:
      - webnet
  portainer:
    image: portainer/portainer
    ports:
            - "9000:9000"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    deploy:
      placement:
        constraints: [node.role == manager]
    networks:
      - webnet
networks:
  webnet:

```
**Com que el servei web no té cap restricció i té 2 rèpliques, l'altre ordinador pot veure la pàgina web**


## PER INICIAR EL SWARM (ordre que fa manager)

```
docker swarm init -> per iniciar el swarm
Swarm initialized: current node (scpdl9jpp1os5rh9cxszsi5j0) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-350xso64zjkzqbzy6o0eruwtml5h77wnopp5d1a4jr9qmmsss7-1bs0knzgs28m4kxn1lab7iaif 192.168.1.132:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

## POSSEM NOM LES NOSTRES CONTENIDORS ("myapp...")

**Ordre que fer a MANAGER**

```
$ docker stack deploy -c docker-compose.yml  myapp
Creating network myapp_webnet
Creating service myapp_portainer
Creating service myapp_web
Creating service myapp_redis
Creating service myapp_visualizer



$ docker stack ps myapp 
ID             NAME                 IMAGE                             NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
p2gbnrvmohlv   myapp_portainer.1    portainer/portainer:latest        debian    Running         Running 14 seconds ago             
p3a49tc0sbok   myapp_redis.1        redis:latest                      debian    Running         Running 17 seconds ago             
iaqabmj7m5ss   myapp_visualizer.1   dockersamples/visualizer:stable   debian    Running         Running 15 seconds ago             
ipimvrdgbe3y   myapp_web.1          kiliangp/getstarted:comptador     debian    Running         Running 17 seconds ago             
1r9t0t63rx4e   myapp_web.2          kiliangp/getstarted:comptador     debian    Running         Running 17 seconds ago
```
## VEIEM ELS SERVEIS QUE TENIM ACTIVATS
```
docker stack services myapp -> podem veure els diferents serveis que tenim.

ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
90nxiysu29hy   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
jhib6109gbzo   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
syfymlmrnkvd   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
l3tfq4k7vj8f   myapp_web          replicated   2/2        kiliangp/getstarted:comptador     *:80->80/tcp

```

## PER ESBORRAR ELS SERVEIS QUE TENIM ACTIVATS

```
docker stack rm myapp

Removing service myapp_portainer
Removing service myapp_redis
Removing service myapp_visualizer
Removing service myapp_web
Removing network myapp_webnet

```
## ORDRE DEPLOY

<br>

**Podem recalcular i reorganitzar els canvis després de fer modificacions al fitxer de docker-compose amb l'ordre deploy, per exemple:**

```
docker stack services myapp
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
i0z2burpoiz5   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
xdany37m3yvp   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
53td9zk37x2c   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
tenastesmcn0   myapp_web          replicated   2/2        kiliangp/getstarted:comptador     *:80->80/tcp

Podem veure que el nñumero de rèpliques és "2".
-----------

vim docker-compose.yml

version: "3" -> hem modificat el número de rèpliques
services:
  web:
    image: kiliangp/getstarted:comptador  
    deploy:
      replicas: 3
      restart_policy:
        condition: on-failure
      resources:
        limits:
          cpus: "0.1"
          memory: 50M

----
Utilitzem l'ordre:

docker stack deploy -c docker-compose.yml myapp

Updating service myapp_portainer (id: i0z2burpoiz5cy1r9o0nuibja)
Updating service myapp_web (id: tenastesmcn0xkukjh4qzhlv2)
Updating service myapp_redis (id: xdany37m3yvpgc5j4br1ei5lf)
Updating service myapp_visualizer (id: 53td9zk37x2cc5omquu5bh95n)

---
Si comprovem els serveis que està actius, podem veure que el número de rèpliques s'ha modificat:

docker stack services myapp
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
i0z2burpoiz5   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
xdany37m3yvp   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
53td9zk37x2c   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
tenastesmcn0   myapp_web          replicated   3/3        kiliangp/getstarted:comptador     *:80->80/tcp

```

**Podem fer-ho al revés, és a dir, canviar de 3 a 2 rèpliques:**

```
docker stack services myapp
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
i0z2burpoiz5   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
xdany37m3yvp   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
53td9zk37x2c   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
tenastesmcn0   myapp_web          replicated   3/2   <--  kiliangp/getstarted:comptador     *:80->80/tcp

```

## SCALE SERVICES

**Estàs indicant que desitges tenir X INSTÀNCIES d'un servei que has indicat. Això pot ser útil per dividir la carga de treball, millorar la disponibilitat.**

```
docker service scale myapp_web=4
myapp_web scaled to 4
overall progress: 4 out of 4 tasks 
1/4: running   [==================================================>] 
2/4: running   [==================================================>] 
3/4: running   [==================================================>] 
4/4: running   [==================================================>] 
verify: Service converged 


Podem veure que tenir 4 rèpliques del contenidor myapp_web:

ocker ps
CONTAINER ID   IMAGE                             COMMAND                  CREATED              STATUS              PORTS                          NAMES
a2175b287336   kiliangp/getstarted:comptador     "python app.py"          14 seconds ago       Up 12 seconds       80/tcp                         myapp_web.4.myiqnrg67sw6besft6dwitltr
e16b2a7af404   kiliangp/getstarted:comptador     "python app.py"          About a minute ago   Up About a minute   80/tcp                         myapp_web.3.zj0c057trfub3cf2m3o2mys44
584d54c80aa4   portainer/portainer:latest        "/portainer"             23 minutes ago       Up 23 minutes       8000/tcp, 9000/tcp, 9443/tcp   myapp_portainer.1.ifvrft308a5j6evs1wle9iohy
5b030c756510   dockersamples/visualizer:stable   "npm start"              23 minutes ago       Up 23 minutes       8080/tcp                       myapp_visualizer.1.hozh5s87zece8kvq5ly8bftlb
b4bb3ef37a5c   redis:latest                      "docker-entrypoint.s…"   23 minutes ago       Up 23 minutes       6379/tcp                       myapp_redis.1.mima2ya84ghakx5k3zlqbyn00
c5b0e6976f99   kiliangp/getstarted:comptador     "python app.py"          23 minutes ago       Up 23 minutes       80/tcp                         myapp_web.2.9d743wl1nuwl4n8v6d03lbrij
cac9176b6413   kiliangp/getstarted:comptador     "python app.py"          23 minutes ago       Up 23 minutes       80/tcp                         myapp_web.1.oamjmv8gna8w8mco6vxwd492c

``` 

