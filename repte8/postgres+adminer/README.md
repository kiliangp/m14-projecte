# M14-REPTE8. DOCKER COMPOSE: iniciació

Implementar els següents exemples de docker-compose:
* Exemple web + portainer
* Exemple net + portainer
* Exemple web + net + portainer
* Exemple postgres + adminer
* Exemple ldap + phpldapadmin

## DOCKER-COMPOSE.YML
```
version: "3"
services:
  db:
    image: kiliangp/postgres-db:latest
    restart: always
    environment:
      POSTGRES_PASSWORD: passwd
      POSTGRES_DB: training
    networks:
        - mynet
  adminer:
    image: adminer
    restart: always
    ports:
      - 8080:8080
    networks:
      - mynet
networks:
  mynet:

```

* Utilitzem el fitxer **yaml** per fer el "docker-compose amb la següent ordre (l'engeguem):
```
docker compose -f docker-compose.yml up -d
```
<br>
Per apagar les dues imatges:

```
docker compose down
```

## COMPROVACIONS

1. En el navegador busques:
* http://localhost:8080 -> port del adminer -> visualitzador
2. Omples les següents dades:
* System -> PostgresSQL
* Server -> db 
* Username -> postgres
* Password -> passwd
* Database -> training

<br>
Podrem veure 5 taules (entrenadors,equips,jugadors,lligues,partits).
