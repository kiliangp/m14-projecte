#! /bin/bash
# PUBLIC
#######################
mkdir /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

#######################
# CONFIGURACIÓ DEL SAMBA
cp /opt/docker/smb.conf /etc/samba/smb.conf

# Creació d'usuari per el samba 
useradd -m -s /bin/bash kilian
echo -e "kilian\nkilian" |smbpasswd -a kilian

# Activem els serveis
/usr/sbin/smbd 
/usr/sbin/nmbd -F 
